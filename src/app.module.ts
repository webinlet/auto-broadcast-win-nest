import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ObsService } from './service/obs/obs.service';
import { ObsModule } from './module/obs/obs.module';

@Module({
  imports: [ObsModule],
  controllers: [AppController],
  providers: [AppService, ObsService],
})
export class AppModule {}
