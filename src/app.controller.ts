import {
  Controller,
  Get,
  Post,
  // NotFoundException,
  Body,
  Query,
  InternalServerErrorException,
} from '@nestjs/common';
import { AppService } from './app.service';
import { ObsService } from 'src/service/obs/obs.service';
import OBSWebSocket from 'obs-websocket-js';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly obsService: ObsService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  /**
   * OBSサーバーに接続テストする
   */
  @Get('obs_connect_test')
  async getObsConnectTest(
    @Query('address') address: string,
    @Query('password') password: string,
  ): Promise<any> {
    try {
      const obs = await this.obsService.connectServer({
        address,
        password,
      });
      this.obsService.disconnectServer(obs);
      return {
        success: true,
        message: '接続に成功しました。',
      };
    } catch (err) {
      throw new InternalServerErrorException(err.error);
    }
  }

  /**
   * 配信を準備する。配信中の場合は何もしない
   */
  @Post('obs_ready_streaming')
  async postRadyStreaming(
    @Body('address') address: string,
    @Body('password') password: string,
    @Body('rtmp_server') rtmpServer: string,
    @Body('rtmp_key') rtmpKey: string,
    @Body('file_path') filePath: string,
  ): Promise<any> {
    let obs: OBSWebSocket | null = null;
    try {
      // 接続
      obs = await this.obsService.connectServer({
        address,
        password,
      });

      const getStreamingStatusResult = await obs.send('GetStreamingStatus');
      if (!getStreamingStatusResult.streaming) {
        // ストリームキーの設定
        await this.obsService.setStreamSettings(obs, {
          rtmpServer,
          rtmpKey,
        });

        // ソースリストのクリア
        await this.obsService.clearSourcesList(obs);

        // ソースの作成
        await this.obsService.createSource(obs, {
          filePath,
        });
      }

      // 接続終了
      this.obsService.disconnectServer(obs);

      return {
        success: true,
        message: !getStreamingStatusResult.streaming
          ? '配信準備に成功しました。'
          : '配信中の為何も実行しませんでした。',
      };
    } catch (err) {
      this.obsService.disconnectServer(obs);
      throw new InternalServerErrorException(err.error);
    }
  }

  /**
   * 配信を開始する。予め準備をする必要がある。配信中は何もしない。
   */
  @Post('obs_start_streaming')
  async postStartStreaming(
    @Body('address') address: string,
    @Body('password') password: string,
  ): Promise<any> {
    let obs: OBSWebSocket | null = null;
    try {
      // 接続
      obs = await this.obsService.connectServer({
        address,
        password,
      });

      const getStreamingStatusResult = await obs.send('GetStreamingStatus');
      if (!getStreamingStatusResult.streaming) {
        // 配信開始
        await this.obsService.startStreaming(obs);
      }

      // 接続終了
      this.obsService.disconnectServer(obs);

      return {
        success: true,
        message: !getStreamingStatusResult.streaming
          ? '配信に成功しました。'
          : '既に配信してます。',
      };
    } catch (err) {
      this.obsService.disconnectServer(obs);
      throw new InternalServerErrorException(err.error);
    }
  }

  /**
   * 配信を終了する。ただ単に終了する。終了している場合は何もしない
   */
  @Post('obs_stop_streaming')
  async postStopStreaming(
    @Body('address') address: string,
    @Body('password') password: string,
  ): Promise<any> {
    let obs: OBSWebSocket | null = null;
    try {
      // 接続
      obs = await this.obsService.connectServer({
        address,
        password,
      });

      const getStreamingStatusResult = await obs.send('GetStreamingStatus');
      if (getStreamingStatusResult.streaming) {
        // ストリーミング配信を停止する
        await this.obsService.stopStreaming(obs);
      }

      return {
        success: true,
        message: getStreamingStatusResult.streaming
          ? '配信の停止に成功しました。'
          : '既に配信停止しています。',
      };
    } catch (err) {
      this.obsService.disconnectServer(obs);
      throw new InternalServerErrorException(err.error);
    }
  }
}
