import { Injectable, InternalServerErrorException } from '@nestjs/common';
const OBSWebSocket = require('obs-websocket-js');
import ObsWebSocket from 'obs-websocket-js';

const mediaName = 'media-source';

@Injectable()
export class ObsService {
  /**
   * OBSサーバーに接続する
   *
   * @access public
   * @param {
   *  address: string;
   *  password: string;
   * }
   * @returns {Promise<ObsWebSocket>}
   * @throws {Error}
   */
  public async connectServer({
    address,
    password,
  }: {
    address: string;
    password: string;
  }): Promise<ObsWebSocket> {
    try {
      const obs = new OBSWebSocket();
      await obs.connect({
        address,
        password,
      });
      return obs;
    } catch (err: any) {
      throw err;
    }
  }

  /**
   * OBSサーバーの接続を閉じる
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @returns {void}
   */
  public disconnectServer(obs: ObsWebSocket): void {
    if (obs) {
      obs.disconnect();
    }
  }

  /**
   * ストリームキーを設定する
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @param {
   *  rtmpServer: string;
   *  rtmpKey: string;
   * }
   * @returns {Promise<void>}
   */
  public async setStreamSettings(
    obs: ObsWebSocket,
    {
      rtmpServer,
      rtmpKey,
    }: {
      rtmpServer: string;
      rtmpKey: string;
    },
  ): Promise<void> {
    await obs.send('SetStreamSettings', {
      type: 'rtmp_custom',
      settings: {
        use_auth: false,
        server: rtmpServer,
        key: rtmpKey,
      },
      save: true,
    });
  }

  /**
   * ソースリストをクリアする
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @returns {Promise<any>}
   */
  public async clearSourcesList(obs: ObsWebSocket): Promise<void> {
    // 現在のシーンの取得と、それ以外を削除
    const currentScene = await this.getCurrentScene(obs);

    // 現在のシーンのソースを取得
    const getSceneItemPropertiesResult = await obs.send('GetSceneItemList', {
      sceneName: currentScene.name,
    });
    for (const i in getSceneItemPropertiesResult.sceneItems) {
      // シーンアイテムを削除
      const sceneItem = getSceneItemPropertiesResult.sceneItems[i];
      await obs.send('DeleteSceneItem', {
        item: {
          name: sceneItem.sourceName,
          id: sceneItem.itemId,
        },
      });

      // ※シーンアイテムを削除してからソースが削除されるまでにラグがある
      const waitLimitCnt = 100;
      for (let i = 0; i < waitLimitCnt; i++) {
        // 削除したはずのソースが無いか確認
        const currentSources = await obs.send('GetSourcesList');
        if (
          currentSources.sources.filter(
            (source) => source.name === sceneItem.sourceName,
          ).length === 0
        ) {
          break;
        }
        // 存在する場合は100ms待機
        await new Promise((resolve) => {
          setTimeout(() => {
            resolve(true);
          }, 100);
        });
      }
    }
  }

  /**
   * ソースを作成する
   *
   * filePath=broadcast-test-0917\10_090.mp4
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @param {
   *  filePath: string;
   * }
   * @returns {Promise<any>}
   */
  public async createSource(
    obs: ObsWebSocket,
    {
      filePath,
    }: {
      filePath: string;
    },
  ): Promise<void> {
    // 現在のシーンの取得と、それ以外を削除
    const currentScene = await this.getCurrentScene(obs);

    // ソース作成
    await obs.send('CreateSource', {
      sourceName: mediaName,
      sourceKind: 'ffmpeg_source',
      sceneName: currentScene.name,
      sourceSettings: {
        buffering_mb: 2,
        clear_on_media_end: true,
        is_local_file: true,
        linear_alpha: false,
        local_file: `G:\\マイドライブ\\${filePath}`,
        looping: false,
        reconnect_delay_sec: 10,
        restart_on_activate: false,
        speed_percent: 100,
      },
    });
    await obs.send('StopMedia', {
      sourceName: mediaName,
    });
  }

  /**
   * ストリーミング配信を開始する
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @returns {Promise<any>}
   */
  public async startStreaming(obs: ObsWebSocket): Promise<void> {
    await obs.send('RestartMedia', {
      sourceName: mediaName,
    });
    await obs.send('StartStreaming', {});
  }

  /**
   * ストリーミング配信を停止する
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @returns {Promise<any>}
   */
  public async stopStreaming(obs: ObsWebSocket): Promise<void> {
    await obs.send('StopStreaming');
    await obs.send('StopMedia', {
      sourceName: mediaName,
    });
  }

  /**
   * 現在のシーンの取得と、それ以外を削除
   *
   * @access public
   * @param {ObsWebSocket}  obs
   * @returns {Promise<ObsWebSocket.Scene>}
   */
  public async getCurrentScene(obs: ObsWebSocket): Promise<ObsWebSocket.Scene> {
    const getSceneListResult = await obs.send('GetSceneList');
    const currentScene = getSceneListResult.scenes.find(
      (scene) => scene.name === getSceneListResult['current-scene'],
    );
    if (!currentScene) {
      throw new InternalServerErrorException(
        'ソースクリア時にカレントシーンの取得に失敗しました。',
      );
    }
    const otherScenes = getSceneListResult.scenes.filter(
      (scene) => scene.name !== getSceneListResult['current-scene'],
    );
    for (const i in otherScenes) {
      const otherScene = otherScenes[i];
      await obs.send('RemoveSceneTransitionOverride', {
        sceneName: otherScene.name,
      });
    }
    return currentScene;
  }
}
