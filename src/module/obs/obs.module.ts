import { Module } from '@nestjs/common';
import { ObsService } from 'src/service/obs/obs.service'

@Module({
  imports: [],
  controllers: [],
  providers: [ObsService],
})
export class ObsModule {}
